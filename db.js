var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//connect to mongodb
mongoose.connect( 'mongodb://localhost/test' );

// Registered user
var userSchema = new Schema ({
  isAdmin: Boolean,
  firstName: String,
  lastName: String,
  email: { type: String, unique: true},
  password: String
});

// Travel plan of user
var planSchema = new Schema ({
  _id: Schema.Types.ObjectId,
  uid: Schema.Types.ObjectId,
  imgUrl: String,
  uid: {
    type: Schema.ObjectId,
    ref: 'users'
  },
  title: String,
  description: String,
  isShare: Boolean,
  rating: Number,
  schedule: [Schema.Types.Mixed]
});

// Destination in the plans
var placeSchema = new Schema ({
  _id: String,
  name: String,
  country: String,
  address: String,
  description: String,
  photos: String,
  rating: Number,
  tags: [],
});

// Comment on particular plans
var commentSchema = new Schema ({
  plan_id: Schema.ObjectId,
  user: {
    type: Schema.ObjectId,
    ref: 'users'
  },
  content : String
});

module.exports.User = mongoose.model('users', userSchema);
module.exports.Plan = mongoose.model('plans', planSchema);
mongoose.model('places', placeSchema);
module.exports.Comment = mongoose.model('comments', commentSchema);
