var login = require('../controller/login-controller');
var register = require('../controller/register-controller');
var post = require('../controller/post-controller');
var plan = require('../controller/plan-controller');
var user = require('../controller/user-controller');
var admin = require('../controller/admin-controller');
var comment = require('../controller/comment-controller');

// route the request to a controller
module.exports = function routes(app) {
  app.use('/login',login);
  app.use('/register',register);
  app.use('/api/user',user);
  app.use('/api/posts',post);
  app.use('/api/plans',plan);
  // app.use('/api/locations',location);
  app.use('/api/admin',admin);
  app.use('/api/comment',comment);
};

module.exports.index = function (req, res) {
  res.render('index', {
    title: 'Home Page',
    project: {name: 'Go Go Tripbook'}
  });
};

module.exports.plans = function (req, res) {
  res.render('plans', {
    title: 'Plans',
    project: {name: 'Go Go Tripbook'}
  });
};

module.exports.places = function (req, res) {
  res.render('places', {
    title: 'Places',
    project: {name: 'Go Go Tripbook'}
  });
};

// module.exports.locations = function (req, res) {
//   res.render('locations', {
//     title: 'Locations',
//     project: {name: 'Go Go Tripbook'}
//   });
// };

module.exports.about = function (req, res) {
  res.render('about', {
    title: 'About Us',
    project: {name: 'Go Go Tripbook'}
  });
};

module.exports.planner = function (req, res) {
  res.render('planner', {
    title: 'Planner',
    project: {name: 'Go Go Tripbook'}
  });
};

module.exports.login = function (req, res) {
  res.render('login', {
    title: 'Login',
    project: {name: 'Go Go Tripbook'}
  });
};

module.exports.register = function (req, res) {
  res.render('register', {
    title: 'Register',
    project: {name: 'Go Go Tripbook'}
  });
};

module.exports.add_place = function (req, res) {
  res.render('admin/add_place', {title: 'Admin add plan'})
};

module.exports.view_plan = function (req, res) {
  res.render('view_plan', {
    title: 'View Plans',
    project: {name: 'Go Go Tripbook'}
  });
};

module.exports.search = function (req, res) {
  res.render('search', {});
};

module.exports.view_place = function (req, res) {
  res.render('view_place', {});
};

//profile module
module.exports.dashboard = function (req, res) {
  res.render('dashboard', {
    title: 'dashboard',
    project: {name: 'Go Go Tripbook'}
  });
};

module.exports.info = function (req, res) {
  res.render('tabs/info', {
    title: 'Info',
    project: {name: 'Go Go Tripbook'}
  });
};

module.exports.myplan = function (req, res) {
  res.render('tabs/myplan', {
    title: 'My Plan',
    project: {name: 'Go Go Tripbook'}
  });
};

module.exports.page = function (req, res) {
  res.render('tabs/page', {
    title: 'Page',
    project: {name: 'Go Go Tripbook'}
  });
};

//admin page

module.exports.admin = function (req, res) {
  res.render('admin', {
    title: 'admin',
    project: {name: 'Go Go Tripbook'}
  });
};

module.exports.add_place = function (req, res) {
  res.render('admin/add_place', {
    title: 'add_place',
    project: {name: 'Go Go Tripbook'}
  });
};

module.exports.users_info = function (req, res) {
  res.render('admin/users_info', {
    title: 'users_info',
    project: {name: 'Go Go Tripbook'},
  });
};

module.exports.page2 = function (req, res) {
  res.render('admin/page2', {
    title: 'page2',
    project: {name: 'Go Go Tripbook'}
  });
};
