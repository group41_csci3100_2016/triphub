var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var assert = require('assert');
var mongoose = require('mongoose');
var sessions = require('client-sessions');
var bcrypt = require('bcryptjs');

// database functinalities
var db = require('./db.js');
var User = mongoose.model('users');
var Plan = mongoose.model('plans');
var Place = mongoose.model('places');
////////////////////////
// Define Router Paths //
////////////////////////
var routes = require('./routes/index');

var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//app.locals.pretty = true;

//middleware
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//define cookie for user
app.use(sessions({
  cookieName: 'session',
  secret: 'jkfhadksfjhsdilofy3u34hyiu',
  duration: 30 * 60 * 1000,
  activaDuration: 5 * 60 * 1000,
}));

app.use(function(req, res, next) {
  if (req.session && req.session.user) {
    User.findOne({email: req.session.user.email}, function(err, user) {
      if (user) {
        req.user = user;
        delete req.user.password;
        req.session.user = req.user;
        res.locals.user = req.user;
      }
      next();
    });
  } else {
    next();
  }
});

function requireLogin(req, res, next) {
  if (!req.user) {
    console.log("not log in");
    // alert("Please login first!");
    res.redirect('/login');
  } else {
    next();
  }
};

// for API call use
// app.use(function (req, res, next) {
//     // Website you wish to allow to connect e.g. http://localhost:3000
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     // Request methods you wish to allow
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//     // Request headers you wish to allow
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//     // Set to true if you need the website to include cookies in the requests sent
//     // to the API (e.g. in case you use sessions)
//     res.setHeader('Access-Control-Allow-Credentials', true);
//     // Pass to next layer of middleware
//     next();
// });

////////////////////////////////
// Routing Pages to functions //
////////////////////////////////
routes(app);
app.get('/', routes.index);
app.get('/plans', routes.plans);
app.get('/places', routes.places);
app.get('/about', routes.about);
app.get('/planner', requireLogin, routes.planner);
app.get('/login', routes.login);
app.get('/register', routes.register);
// app.get('/admin/add_place', routes.add_place);
app.get('/view_plan', routes.view_plan);
//user profile file
app.get('/myprofile', routes.dashboard);
app.get('/tabs/info', routes.info);
app.get('/tabs/myplan', routes.myplan);
app.get('/tabs/page', routes.page);
app.get('/admin', routes.admin);
app.get('/admin/add_place', routes.add_place);
app.get('/admin/page2', routes.page2);
app.get('/admin/users_info', routes.users_info);
app.get('/search', routes.search);
app.get('/view_place', routes.view_place);
// find all locations
// app.get('/destinations', routes.locations);

// add plan: function temporarily written here
//
// HTTP POST data should include a single object with following fields
// example:
// {
//   user_id: '57092eebed7bf550155fdaee', // this uid is a ObjectId string, and will be turn into ObjectId in backend
//   description: 'This is the first plan!',
//   schedule: [
//     [
//       {place_id: 123, time: '10:00'},
//       {place_id: 124, time: '10:30'}
//     ],
//     [
//       {place_id: 123, time: '10:00'},
//       {place_id: 125, time: '11:30'}
//     ]
//   ]
// }
//
//

// Find plan by id
//
var Plan = require('./db').Plan;
app.get('/plan/:id', function(req,res) {
  // res.send(req.params.id);
  Plan.findOne({_id:req.params.id})
  .populate('uid')
  .exec(function(err, result){
    if (err || !result){
      res.send("Can't find plan!");
    }
    else {
      res.send(result);
    }
  });
});
// End

// Find all places
//
var Locations = require('./db').Place;
app.get('/api/locations', function(req,res) {
  // res.send(req.params.id);
  Locations.find().exec(function(err, result){
    if (err || !result){
      res.send("Can't find plan!");
    }
    else {
      res.send('hello');
    }
  });
});
// End


app.post('/api/add_plan', function (req, res) {
  // this function will add plan if not exist,
  // update plan if already exist
  var record = req.body;
  if(record._id){
    console.log('have record_id');
    record._id = new mongoose.Types.ObjectId(record._id); // generate ObjectId for the record
  } else {
    console.log('no record_id!!!')
    console.log('rec=',record._id);
    record._id = new mongoose.Types.ObjectId; // generate ObjectId for the record
  }
  record.uid = new mongoose.Types.ObjectId(record.uid); // turn string to ObjectId
  Plan.update({_id: record._id}, record, {upsert: true},function(err, doc) {
    if(err) res.json(err);
    if(!doc) res.json(err);
    else res.json(doc);
  })

  //add
})

app.post('/api/find_plans_with_placeid', function (req, res) {
  // this function will add plan if not exist,
  // update plan if already exist
  var place_id = req.body.place_id;
  console.log(place_id);
  Plan.find({schedule: {$elemMatch: {$elemMatch: {place_id: place_id}}}})
  .populate('uid')
  .exec(function(err, doc) {
    if(err) res.json(err);
    if(!doc) res.json(err);
    else res.json(doc);
  })

  //add
})

app.post('/api/admin/add_place', function (req, res) {
  var record = req.body;
  console.log("record:",record);
  record.forEach(function (place) {
    console.log("each palce", place);
    Place.update({_id: place._id}, place, {upsert: true},function(err, doc) {
      if(err) res.json(err);
      if(!doc) res.json(err);
    })
  });
  res.json("added place");

});

app.post('/api/search', function (req, res) {
  var query = req.body; // query is an object like {field: {$regex: keyword}}
  Place.find(query,function (err, doc) {
    if(doc) {
      res.json(doc);
    } else {
      res.json({status: 'empty'});
    }
  })

});

app.get('/api/places', function (req, res) {
  Place.find(function (err, doc) {
    if(doc) {
      res.send(doc);
    } else {
      console.log(err);
    }
  });
});

app.post('/api/place_detail', function (req, res) {
  var place_id = req.body.place_id;
  Place.findOne({_id: place_id},function (err, doc) {
    if(doc) {
      res.json(doc);
    } else {
      res.json({status: 'empty'});
    }
  })

});

app.get('/api/get_country_name', function (req,res) {
  Place.find().distinct("country", function (err, doc) {
    if(doc) {
      res.json(doc);
    } else {
      res.json({status: 'empty'});
    }
  })
})

app.get('/find/:id', function (req, res) {
  plan.find({schedule : {$elemMatch: {$elemMatch: {place_id: +req.params.id}}}}, function (err, plan) {
    if(err){
      console.log(err);
      res.send("err");
    } else {
      res.json(plan);
    }
  })
});

app.get('/info/:user', function (req, res) {
  User.find({firstName: req.params.user}, function (err, data) {
    if(err){
      console.log("404");
    } else {
      res.send(data);
    }
  });
});

// app.get('/testing/:postId', function(req, res){
//   Post.find({_id: req.params.postId}, function(err, posts) {
//     //Post.populate(posts, {path: 'user'}, function(err, posts) {
//       res.send(posts);
//     //});
//   });
// });




// catch 404 and forward to error handler

// app.use(function (req, res, next) {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

//mongoose function
app.get('/dashboard', requireLogin, function (req, res) {
  console.log("logged in");
  res.render('dashboard');
});

app.get('/logout', function(req, res) {
  req.session.reset();
  res.redirect('/login');
});

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
