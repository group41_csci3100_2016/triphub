var mongoose = require('mongoose');
var User = require('../db').User;
var bcrypt = require('bcryptjs');
var sessions = require('client-sessions');
var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  User.find(function(err, results){
    res.send( {users: results} );
  });
});

router.put('/:id', function(req, res) {
  var id = req.params.id;
  console.log(req.body.pw);
  var hash = bcrypt.hashSync(req.body.pw, bcrypt.genSaltSync(10));
  User.update({ _id: mongoose.Types.ObjectId(id)}, {
    $set: { password: hash }
  }, function(err) {
    if (err) {
      console.log(err);
    }
    res.send("password reset");
  })
});

router.delete('/:id', function(req, res) {
  var id = req.params.id;
  User.remove({ _id: mongoose.Types.ObjectId(id)}, function(err) {
    if (err) {
      console.log(err);
    }
    res.send("user remove");
  });
});

module.exports = router;
