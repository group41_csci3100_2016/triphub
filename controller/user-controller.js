var mongoose = require('mongoose');
var User = require('../db').User;
var Plan = require('../db').Plan;
var bcrypt = require('bcryptjs');
var sessions = require('client-sessions');
var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  User.findOne({_id: req.session.user._id}, function(err, results){
    res.send({ user: results});
  });
});

router.get('/plan', function(req, res) {
  Plan.find({uid: req.session.user._id}, function(err, results){
    res.send( {userplans: results} );
  });
});

//update firstname
router.put('/1/:id', function(req, res) {
  var id = req.params.id;
  console.log(req.body.firstName);
  if (req.body.firstName != null) {
    console.log("not null");
    User.update({ _id: mongoose.Types.ObjectId(id)}, {
      $set: { firstName: req.body.firstName }
    }, function(err) {
      if (err) {
        console.log(err);
      }
      res.send("firstname updated");
    });
  } else {
    res.send(404);
  }
});

//update lastname
router.put('/2/:id', function(req, res) {
  var id = req.params.id;
  if (req.body.lastName != null) {
    User.update({ _id: mongoose.Types.ObjectId(id)}, {
      $set: { lastName: req.body.lastName }
    }, function(err) {
      if (err) {
        console.log(err);
      }
      res.send("lastname updated");
    });
  } else {
    res.send(404);
  }
});

router.put('/3/:id', function(req, res) {
  var id = req.params.id;
  console.log(req.body.oldpw);
  User.findOne({_id: req.session.user._id}, function(err, user){
    if (bcrypt.compareSync(req.body.oldpw, user.password) && req.body.newpassword == req.body.renewpassword) {
      console.log("correct pw");
      var hash = bcrypt.hashSync(req.body.newpassword, bcrypt.genSaltSync(10));
      User.update({ _id: mongoose.Types.ObjectId(id)}, {
        $set: { password: hash }
      }, function(err) {
        if (err) {
          console.log(err);
        }
        res.send("password updated");
    });
  } else {
    res.send(404);
  }
  })
});


module.exports = router;
