var mongoose = require('mongoose');
var User = require('../db').User;
var express = require('express');
var bcrypt = require('bcryptjs');
var router = express.Router();

router.post('/', function(req, res) {
  //generate hash of the password
  var hash = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
  var user = new User({
    isAdmin: 0,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: hash,
  });
  user.save(function(err){
    if (err) {
      console.log("try");
      var error = 'Try again';
      if (err.code == 11000) {
        error = "Email exists";
      }
      res.render('register', { error: error});
    } else {
      res.redirect('/login');
    }
  });
});

module.exports = router;
