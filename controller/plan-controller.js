var mongoose = require('mongoose');
var Plan = require('../db').Plan;
var express = require('express');
var router = express.Router();
    
router.get('/', function(req, res){
    Plan.find(function(err, plans){
        Plan.populate(plans, {path: 'user'}, function(err, results) {
            if (err) {
                console.log(err);
            }
            console.log(results);
            res.send({ plans: results});
        });
    });
});

module.exports = router;
