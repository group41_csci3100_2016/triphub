var mongoose = require('mongoose');
var User = require('../db').User;
var bcrypt = require('bcryptjs');
var sessions = require('client-sessions');
var express = require('express');
var router = express.Router();

router.post('/', function(req, res) {
  User.findOne({email: req.body.email}, function(err, user){
    if(!user) {
      res.render('login', { error: 'Invaild email or password.'});
    } else{
      if (bcrypt.compareSync(req.body.password, user.password) && user.isAdmin == true) {
        req.session.user = user;
        res.cookie('uid',user._id.toString());
        res.redirect('/admin');
      }else if (bcrypt.compareSync(req.body.password, user.password)) {
        req.session.user = user;
        res.cookie('uid',user._id.toString()); // store plain-text uid to cookie for angular use
        res.redirect('/dashboard');
      } else {
        res.render('login', { error: 'Invaild email or password.'});
      }
    }
  });
});

module.exports = router;
