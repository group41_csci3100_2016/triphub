var mongoose = require('mongoose');
var Post = require('../db').Plan;
var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  Post.find(function(err, posts) {
    Post.populate(posts, {path: 'user'}, function(err, results) {
      if (err) {
        console.log(err);
      }
      console.log(results);
      res.send({ posts: results});
    });
  });
});

router.get('/:userId', function(req, res) {
  Post.find({_id: req.params.userId},function(err, posts) {
    Post.populate(posts, {path: 'user'}, function(err, results) {
      if (err) {
        console.log(err);
      }
      console.log(results);
      res.send(results);
      //res.send({ posts: results});
    });
  });
});

module.exports = router;
