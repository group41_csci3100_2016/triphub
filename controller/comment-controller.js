var mongoose = require('mongoose');
var Comment = require('../db').Comment;
var bcrypt = require('bcryptjs');
var sessions = require('client-sessions');
var express = require('express');
var router = express.Router();

router.get('/:id', function(req, res) {
  Comment.find({plan_id: mongoose.Types.ObjectId(req.params.id) }, function(err, results) {
    Comment.populate(results, {path: 'user'}, function(err, results) {
      res.send( {comments: results} );
    })
  })
});

router.post('/:id', function(req, res) {
  console.log(req.params.id);
  console.log(req.session.user._id);
  console.log(mongoose.Types.ObjectId(req.params.id));
  console.log(mongoose.Types.ObjectId(req.session.user._id));
  var comment = new Comment({
    plan_id: mongoose.Types.ObjectId(req.params.id),
    user: mongoose.Types.ObjectId(req.session.user._id),
    content: req.body.comment
  });
  comment.save(function(err){
    if (err) {
      console.log("try");
    } else {
      res.redirect('/view_plan#/?plan_id=' + req.params.id);
    }
  });
});

module.exports = router;
