angular.module('TriphubApp',['ui.router','ui.bootstrap'])

.config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider) {
  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('general', {
      url: '/',
      templateUrl: 'tabs/info',
      controller: 'infoCtrl'
    })
    .state('myplan', {
      url: '/myplan',
      templateUrl: 'tabs/myplan',
      controller: 'myplanCtrl'
    })
    .state('pages', {
      url: '/pages',
      templateUrl: 'tabs/page'
    })
}])

.controller('infoCtrl', function($scope, $http) {
  $scope.isEditing = 0;
  //get user info from db
  function getUser() {
    $http.get('/api/user').success(function(response) {
      console.log("user info got");
      $scope.user = response.user;
    });
  }

  getUser();

  $scope.onEditClick = function (val) {
    console.log(val);
    $scope.error = '';
    $scope.isEditing = val;
  };
  $scope.onCancelClick = function (val) {
    console.log(val);
    $scope.isEditing = val;
  };
  $scope.updateUser = function (user) {
    console.log(user._id);
    if ($scope.isEditing == 1) {
      $http.put(`/api/user/1/${user._id}`, {firstName: $scope.firstname}).success(function() {
        getUser();
        $scope.firstname = '';
        $scope.isEditing = 0;
        window.location.reload();
      }).error(function() {
        console.log("error");
        $scope.error = 'Please input firstname';
      });
    } else if ($scope.isEditing == 2) {
      $http.put(`/api/user/2/${user._id}`, {lastName: $scope.lastname}).success(function() {
        getUser();
        $scope.lastname = '';
        $scope.isEditing = 0;
      }).error(function() {
        console.log("error");
        $scope.error = 'Please input lastname';
      });
    }
     else if ($scope.isEditing == 3) {
      $http.put(`/api/user/3/${user._id}`, {oldpw: $scope.oldpassword, newpassword: $scope.newpassword, renewpassword: $scope.renewpassword}).success(function() {
        getUser();
        $scope.oldpassword = '';
        $scope.newpassword = '';
        $scope.renewpassword = '';
        $scope.isEditing = 0;
      }).error(function() {
        console.log("error");
        $scope.error = 'Cannot change password';
      })
    }
  };
})

.controller('myplanCtrl', function($scope, $http) {
  $http.get('/api/user/plan').success(function(response) {
    $scope.userplans = response.userplans;
  });
})

.filter('startForm', function() {
  return function(data, start) {
    return data.slice(start);
  }
})
