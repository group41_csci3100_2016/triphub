var advance_options = ["abc", "abc"];
var advance_upward = "<span id=\"advance_upward\" class=\"glyphicon glyphicon-menu-down advance\"></span>";
var advance_downward = "<span id=\"advance_downward\" class=\"glyphicon glyphicon-menu-up advance\"></span>";

angular.module('TriphubApp')
.controller('IndexController', ['$scope', '$http',function ($scope, $http) {
  $scope.keyword = '';
  $scope.states = [];
  // var base_url = 'http://localhost:3000'
  $http.get('/api/get_country_name')
  .then(function success(result) {
    $scope.states = result.data;
  })

  $scope.search = function () {
     window.location.href = '/search#?keyword=' + $scope.keyword;
  }

  $scope.toggle = false;
  $scope.toggleOption = function () {
    if ($scope.toggle == false) {
      $scope.toggle = true;
    } else {
      $scope.toggle = false;
    }
  }

}]);
