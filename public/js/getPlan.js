var app = angular.module('TriphubApp',['ui.bootstrap']);
app.controller('myCtrl', function($scope, $http) {
  $scope.pageSize = 4;
  $scope.currentPage = 1;
  $http.get('/api/plans').success(function(response) {
      console.log("angular controller");
      console.log(response.plans.length);
      $scope.plans = [];
      // check if it is set to public, add it
      for(var i=0; i<response.plans.length; i++){
        var isShare = response.plans[i].isShare;
        console.log(isShare);
        if(isShare){
          $scope.plans.push(response.plans[i]);
        }
      }
  });
});

app.filter('startForm', function() {
  return function(data, start) {
    return data.slice(start);
  }
});
