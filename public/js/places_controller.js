angular.module('TriphubApp')
.controller('PlacesController', ['$scope', '$http', '$location', '$cookies', function($scope, $http, $location, $cookies) {
  var self = this;
  this.name = 'Hotel';
  this.address = 'asdfadfasdf';
  this.rating = 4.5;
  this.tags = ['123', 'abc', 'hahaha'];
  this.results = [];
  this.cart = [];
  
  $http.get('/api/places').success(function(response) {
    console.log(response);
    self.results = response;
  }, function fail(response) {
    console.log('fail', response);
  });

  this.add_to_cart = function (place_id) {
    if($cookies.getObject('cart'))
      self.cart = $cookies.getObject('cart');

    if(self.cart.indexOf(place_id) == -1) {
      self.cart.push(place_id);
      $cookies.putObject('cart',self.cart);
    }

  }
  this.click = function () {

    console.log($cookies.getObject('cart'));
  }

}])
