// Creates the gservice factory. This will be the primary means by which we interact with Google Maps
angular.module('gservice', [])
    .factory('gservice', function($rootScope){
      var gmap = {}; // gservice factory object to be return by angular

      var map; //google map Object
      var service;

      gmap.init = function (lat, lng) {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: {lat: lat, lng: lng},
            disableDoubleClickZoom: true
        });

        map.addListener('center_changed', function () {
          $rootScope.$emit('gservice:centerChanged',  map.getCenter().toJSON());
        })

        map.addListener('bounds_changed', function () {
          $rootScope.$emit('gservice:boundsChanged');
        })

        //create serivce object
        service = new google.maps.places.PlacesService(map);

        // auto-complete
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });
        var place;
        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          place = autocomplete.getPlace();
          if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            //map.setZoom(17);  // Why 17? Because it looks good.
          }

          createMarker(place);
        });



      }

      //google places search

      gmap.search = function (type) {
        //clear markers before search
        // gmap.clearMarkers();
        var request = {
          bounds: map.getBounds(),
          type: [type]
        }
        service.nearbySearch(request, function(results, status) {
          if (status == google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < 10; i++) {
              createMarker(results[i]);
            }
          }
        });
      }

      var markers = [];

      gmap.createMarkerfromPlaceID = function (place_id) {
        service.getDetails({placeId: place_id}, function(place, status) {
          createMarker(place);
          $rootScope.$emit('gservice:addPlace', place, true);
          map.setCenter(place.geometry.location);
        });
      }


      function createMarker(place) {
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
          map: map,
          position: place.geometry.location
        });
        marker.setIcon(/** @type {google.maps.Icon} */({
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(35, 35)
        }));
        var infowindow = new google.maps.InfoWindow();

        var address = '';
        if (place.address_components) {
          address = [
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || '')
          ].join(' ');
        }
        var img_url = '';
        if(place.hasOwnProperty('photos')){
          img_url = place.photos[0].getUrl({maxWdith:80, maxHeight:80})
        }
        infowindow.setContent('<div><strong>' + place.name + '</strong><br><img src='+ img_url+'>');
        // infowindow.open(map, marker);
        google.maps.event.addListener(marker, 'mouseover', function() {
          infowindow.open(map, this);
        });
        google.maps.event.addListener(marker, 'mouseout', function() {
          infowindow.close();
        });
        google.maps.event.addListener(marker, 'dblclick', function() {
          $rootScope.$emit('gservice:addPlace', place);
        });

        markers.push(marker);
      }

      gmap.clearMarkers = function () {
        markers.forEach(function (m) {
          m.setMap(null);
        });
        markers = [];
      }

      gmap.focusPlace = function (place) {
        map.setCenter(place.geometry.location);
        var infowindow = new google.maps.InfoWindow();
        infowindow.open(map);
        infowindow.setPosition(place.geometry.location);
        infowindow.setContent('<h4>' + place.name + '</h4>');
      }
        // Refresh the page upon window load. Use the initial latitude and longitude
      google.maps.event.addDomListener(window, 'load',
           gmap.init(22.402892150505288,114.12281808750005));

      return gmap;
});
