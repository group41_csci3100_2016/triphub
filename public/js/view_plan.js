// get plan detail

var plan = angular.module('TriphubApp');
plan.controller('myCtrl', ['$anchorScroll', '$scope', '$http', '$location',
    function($anchorScroll, $scope, $http, $location) {
        console.log($location.search().plan_id);
        $http.get('/plan/'+$location.search().plan_id).success(function(response) {
            $scope.plan=response;
            console.log(response);
            console.log("success");

            $scope.schedule = $scope.plan.schedule;
            console.log($scope.schedule);
            console.log($scope.schedule[0][0].name);
            console.log($scope.schedule[0][0].place_id);
        });

        $scope.gotoAnchor = function(x){
            var newHash = 'anchor' + x;
            if($location.hash() !== newHash){
                $location.hash('anchor' + x);
            } else {
                $anchorScroll();
            }
        };
        $http.get('/api/comment/'+$location.search().plan_id).success(function(response) {
            console.log("get comment");
            $scope.comments = response.comments;
        });
    }
]);


/* activate sidebar */
$('#sidebar').affix({
  offset: {
    top: 235
  }
});

/* activate scrollspy menu */
var $body   = $(document.body);
var navHeight = $('.navbar').outerHeight(true) + 10;

$body.scrollspy({
	target: '#leftCol',
	offset: navHeight
});

/* smooth scrolling sections */
$('a[href]').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      console.log("here");
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 50
        }, 1000);
        return false;
      }
    }
});

$(document).ready(function() {
  $('[data-toggle=offcanvas]').click(function() {
    $('.row-offcanvas').toggleClass('active');
  });
});
