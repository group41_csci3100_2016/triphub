var app = angular.module('TriphubApp');

app.controller('getUserCtrl', function($scope, $http) {
  $scope.pageSize = 5;
  $scope.currentPage = 1;
  $scope.test = 'a';
  getUserInfo();

  function getUserInfo() {
    $http.get('/api/admin').success(function(response) {
      $scope.users = response.users;
    });
  }

  $scope.onEditClick = function(user) {
    console.log(user._id);
    $http.put(`/api/admin/${user._id}`, {pw: "0000"}).success(function() {
      getUserInfo();
    });
  }

  $scope.onDeleteClick = function(user) {
    console.log(user._id);
    $http.delete(`/api/admin/${user._id}`).success(function() {
      getUserInfo();
    });
  }
})

app.filter('startForm', function() {
  return function(data, start) {
    return data.slice(start);
  }
})
