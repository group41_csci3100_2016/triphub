angular.module('TriphubApp',['ui.router','ui.bootstrap'])

.config(['$urlRouterProvider', '$stateProvider', function($urlRouterProvider, $stateProvider) {
  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('add_place', {
      url: '/',
      templateUrl: 'admin/add_place',
      controller: 'PlacesController'
    })
    .state('users_info', {
      url: '/users_info',
      templateUrl: 'admin/users_info',
      controller: 'getUserCtrl'
    })
    .state('page2', {
      url: '/page2',
      templateUrl: 'admin/page2'
    })
}])
