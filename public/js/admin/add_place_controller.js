angular.module('TriphubApp')
.controller('PlacesController', ['$scope', '$http', function($scope, $http) {
  var self = this;
  this.place_id = 'ChIJsz7XbJEABDQRhZtEqR96mI0';
  this.description = 'This is a good place';
  this.country = '';

  var service = new google.maps.places.PlacesService(document.createElement('div')); // create place service without a map

  this.crawl = function () {
    service.getDetails({placeId: self.place_id}, function (place, status) {
      console.log(place);
      place.address_components.forEach(function(component) {
        if(component.types.indexOf('country') != -1){
          self.country = component.short_name;
          $scope.$apply();
          return;
        }
      });
      self.place = place;
      $scope.$apply();
    });

  }

  // var base_url = 'http://localhost:3000'
  this.save_to_db = function () {
    var place = {
      _id: self.place.place_id,
      name: self.place.name,
      country: self.country,
      address: self.place.formatted_address,
      description: self.description,
      photos: self.place.photos[0].getUrl({maxWidth: 300, maxHeight: 240}),
      rating: self.place.rating,
      tags: self.place.types,
    };

    $http.post('/api/admin/add_place', place)
    .then(function success(response) {
      console.log('success', response);
    }, function (response) {
      console.log('error!', response);
    })
  }


}])


var hihihi = 'hellowrodl';
