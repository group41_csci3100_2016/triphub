angular.module('TriphubApp')
.controller('ViewPlaceController', ['$scope', '$http', '$location', '$cookies', function($scope, $http, $location, $cookies) {
  var self = this;
  this.results = [];
  this.place = {};

  // var base_url = 'http://localhost:3000';
  this.place_id = $location.search().place_id;

  $http.post('/api/place_detail',{place_id:self.place_id})
  .then(function success(response) {
    console.log(response);
    self.place = response.data;
  }, function fail(response) {
    console.log('fail', response);
  })

  // find plans with place id

  $http.post('/api/find_plans_with_placeid',{place_id:self.place_id})
  .then(function success(response) {
    console.log(response);
    self.results = response.data;
  }, function fail(response) {
    console.log('fail', response);
  })
  //
  // this.add_to_cart = function (place_id) {
  //   if($cookies.getObject('cart'))
  //     self.cart = $cookies.getObject('cart');
  //
  //   if(self.cart.indexOf(place_id) == -1) {
  //     self.cart.push(place_id);
  //     $cookies.putObject('cart',self.cart);
  //   }
  //
  // }
  // this.click = function () {
  //
  //   console.log($cookies.getObject('cart'));
  // }

}])
