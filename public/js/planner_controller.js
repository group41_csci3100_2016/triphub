var gmap = angular.module('TriphubApp')
.controller('MapController', ['$scope', 'gservice', '$rootScope', '$timeout', '$http', '$cookies', '$location',function ($scope, gservice, $rootScope, $timeout, $http, $cookies, $location) {
  var self = this;
  this.plan_id = $location.search().plan_id; //if plan_id is supplied, load the plan
  this.uid = $cookies.get('uid'); // get uid from cookies
  this.schedule = [[]];
  this.places = [];
  this.places_in_schedule = []; // for saving newly added places back to our DB
  this.isShare = false;

  // load plan if plan_id is supplied
  if(self.plan_id){

    $http.get('/plan/'+ self.plan_id)
    .success(function(response) {
      self.schedule=response.schedule;

      self.schedule.forEach(function(day) {
        day.forEach(function(place_obj) {
          // add markers for places in schedule
          gservice.createMarkerfromPlaceID(place_obj.place_id);
        })
      })
      console.log(response);
      console.log("success");


    })
  } else { // if no plan id, load places from cookies
    var cart = $cookies.getObject('cart');
    if(cart) {
      cart.forEach(function (place_id) {
        gservice.createMarkerfromPlaceID(place_id);
      })
    }

  }

  this.onDropComplete= function (data,evt,index){
      newPlace = {name: data.name, place_id: data.place_id, imgUrl: data.photos[0].getUrl({maxWidth: 300, maxHeight: 240})};
      self.schedule[index].push(newPlace);
      addPlacetoPlaceDB(data);
      console.log(self.schedule);
  }
  var service = new google.maps.places.PlacesService(document.createElement('div')); // create place service without a map
  function addPlacetoPlaceDB(place) {
    service.getDetails({placeId: place.place_id}, function(place_detail, status) {
      place_detail.address_components.forEach(function(component) {
        if(component.types.indexOf('country') != -1){
          place_detail.country = component.short_name;
          return;
        }
      });
      var newPlace = {
        _id: place_detail.place_id,
        name: place_detail.name,
        country: place_detail.country,
        address: place_detail.formatted_address,
        description: place_detail.name + "description",
        photos: (place_detail.hasOwnProperty('photos')) ? place_detail.photos[0].getUrl({maxWidth:300, maxHeight: 240}) : 'image/card6.jpg',
        rating: place_detail.rating,
        tags: place_detail.types,
      };
      self.places_in_schedule.push(newPlace);
      console.log("hihi",self.places_in_schedule);
    })
  }

  this.addDay = function () {
    self.schedule.push([]);
    self.n_days++;
  }
  this.removeDay = function (day_index) {
    if(confirm('Are you sure to delete Day ' + (day_index+1) + '?')) {
      self.schedule.splice(day_index,1);
    }
  }

  // listerners for AJAX response events emitted from google maps api
  $rootScope.$on('gservice:centerChanged', function (event,arg) {
    //console.log(arg.lat, arg.lng);
    self.lat  = arg.lat;
    self.lng  = arg.lng;
    if(!$scope.$$phase)
      $scope.$apply();
  });
  $rootScope.$on('gservice:addPlace', function (event, place, noAlert) {
    for (var i = 0; i < self.places.length; i++) {
      if (self.places[i].place_id == place.place_id) {
        if(!noAlert) window.alert(place.name + " has been added already!");
        return null;
      }
    }
    self.places.push(place);
    addtoCookies(place);
    $scope.$apply();
  })

  // cookies related functions
  // used by 'gservice:addPlace' listener, removePlace()
  function addtoCookies(place) {
    var cart = $cookies.getObject('cart');
    // if cart is empty, create one
    if(!cart){
      cart = [];
    }
    if(cart.indexOf(place.place_id) == -1) {
      cart.push(place.place_id);
      $cookies.putObject('cart',cart);
    }
  }
  function removefromCookies(place) {
    var cart = $cookies.getObject('cart');
    var index = cart.indexOf(place.place_id);
    if(index >= 0) {
      cart.splice(index,1);
      $cookies.putObject('cart',cart);
    }
  }
  // end cookies related functions

  // search by text
  this.availableSearchTypes = ['food', 'shop' , 'health', 'park'];
  this.selectedType = 'food';
  this.search = function () {
    gservice.search(self.selectedType);
  }
  // end search by text


  // cross button on place card
  this.removePlace = function (place) {
    if(confirm('Are you sure to delete ' + place.name + '?')) {
      removefromCookies(place);
      var index = self.places.indexOf(place);
      self.places.splice(index,1);
    }
  }
  // clear button
  this.clearAll = function () {
    $cookies.remove('cart');
    self.places = [];
    gservice.clearMarkers();
  }

  // generate color for timeline
  var color = ["#f0ad4e", "#d9534f", "#5bc0de", "#3f903f"];
  this.getColor = function (index) {
    return color[index%4];
  }

  this.focusPlace = function (place) {
    gservice.focusPlace(place);
  }

  // save plan and place to DB
  this.save = function () {
    //test data
    var imgUrl = "";
    if(self.isShare){
      imgUrl = prompt("Please enter imgUrl");
      if(!imgUrl){
        alert("Cannot share the plan without imgUrl!");
        return;
      }
    }
    var uid = $cookies.get('uid')
    if(!uid){
      alert("not logged in!");
      return;
    }

    var plan = {
      _id: self.plan_id,
      uid: uid, // this uid is a string, and will be turn into ObjectId in backend
      imgUrl: imgUrl,
      title: prompt("Please enter title"),
      description: prompt("Please enter description"),
      isShare: self.isShare,
      schedule: JSON.parse(angular.toJson(self.schedule))
    }
    console.log(plan);
    // save plan
    $http.post('/api/add_plan', plan)
    .then(function success(res) {
      console.log('sucessully saved!', res.data);
      window.alert('sucessully saved!');
      self.plan_id = res.data.upserted[0]._id;
      $location.search('plan_id',self.plan_id);
    }, function failed(res) {
      console.log('fail to save', res.status);
    })

    //save place
    $http.post('/api/admin/add_place', self.places_in_schedule)
    .then(function success(res) {
     console.log(res);
    }, function fail(res) {
     console.log('failed', res);
    })
  }


}]);
