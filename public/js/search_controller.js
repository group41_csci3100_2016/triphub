angular.module('TriphubApp')
.controller('SearchController', ['$scope', '$http', '$location', '$cookies', function($scope, $http, $location, $cookies) {
  var self = this;

  this.results = [];
  this.cart = [];

  this.keyword = $location.search().keyword;
  // var base_url = 'http://localhost:3000';
  console.log('keyword: ', self.keyword);
  $http.post('/api/search/',{country:{$regex: self.keyword}})
  .then(function success(response) {
    console.log(response);
    self.results = response.data;
  }, function fail(response) {
    console.log('fail', response);
  })

  this.add_to_cart = function (place_id) {
    if($cookies.getObject('cart'))
      self.cart = $cookies.getObject('cart');

    if(self.cart.indexOf(place_id) == -1) {
      self.cart.push(place_id);
      $cookies.putObject('cart',self.cart);
    }

  }
}])
