# GoGoTripperBook

## Project structure
Refer to `Main_file_list.txt` for project structure guideline
```
1. Index page
2. Browse Plans
3. Browse Places
4. Trip planner
5. View a particular plan
6. View a particular location
7. Search result
8. Sign up
9. Login
10. User dashboard
11. Admin feature
```

## How to start app?
1. (For first time only: `npm install` to install all node packages)
2. Go to the terminal and type: `npm start` to start the app
3. go to http://localhost:3000 using browser or using `curl`

## Database Dump
* Set the path to MongoDB
```
#!bash
//Windows
set PATH=%PATH%;C:\Program Files\MongoDB\Server\3.2\bin
```
```
#!bash
//MAC
export PATH=$PATH:<path to mongodb bin>
```
```
#!bash
mongodump -o "<folder_name>"
```
```
#!bash
mongorestore --dir "<folder_name>"
```

## Useful Tools
### Atom
1. minimap
2. ternjs (code intellegence/autocomplete)
3. atom-jade (jade syntax highlighting)
4. indent-guide-improved
5. markdown preview (pre-installed) (ctrl-shift-M)
6. Drag sth. https://github.com/fatlinesofcode/ngDraggable

## Structure
* app.js: routing URL to functions
  1. step1: define route source file
  2. step2: HTTP method(GET/POST), path, the handler

```
#!javascript

var routes = require('./routes/index');
app.get('/', routes.index);

```


* ./routes/\*.js: all handlers


```
#!javascript

module.exports.index = function (req, res) {
    res.render('jadefile', {object: 'object'});
}
```


* ./views/*.jade: template
